package com.my.olegfp;

import java.util.Scanner;

public class CounterStrike {
    public static void main(String[] args) {
        System.out.println("Введіть назву першої команди: ");
        Scanner scanner = new Scanner(System.in);
        String firstTeamName = scanner.nextLine();

        System.out.println("Введіть кількість фрагів першого гравця команди " + firstTeamName);
        int frag1Team1 = scanner.nextInt();
        while (frag1Team1 < 0) {
            System.out.println("Так не буває, спробуй ще раз!");
            frag1Team1 = scanner.nextInt();
        }

        System.out.println("Введіть кількість фрагів другого гравця команди " + firstTeamName);
        int frag2Team1 = scanner.nextInt();
        while (frag2Team1 < 0) {
            System.out.println("Так не буває, спробуй ще раз!");
            frag2Team1 = scanner.nextInt();
        }

        System.out.println("Введіть кількість фрагів третього гравця команди " + firstTeamName);
        int frag3Team1 = scanner.nextInt();
        while (frag3Team1 < 0) {
            System.out.println("Так не буває, спробуй ще раз!");
            frag3Team1 = scanner.nextInt();
        }

        System.out.println("Введіть кількість фрагів четвертого гравця команди " + firstTeamName);
        int frag4Team1 = scanner.nextInt();
        while (frag4Team1 < 0) {
            System.out.println("Так не буває, спробуй ще раз!");
            frag4Team1 = scanner.nextInt();
        }

        System.out.println("Введіть кількість фрагів п'ятого гравця команди " + firstTeamName);
        int frag5Team1 = scanner.nextInt();
        while (frag5Team1 < 0) {
            System.out.println("Так не буває, спробуй ще раз!");
            frag5Team1 = scanner.nextInt();
        }

        System.out.println("Введіть назву другої команди: ");
        Scanner scanner2;
        scanner2 = new Scanner(System.in);
        String secondTeamName = scanner2.nextLine();

        System.out.println("Введіть кількість фрагів першого гравця команди " + secondTeamName);
        int frag1Team2 = scanner.nextInt();
        while (frag1Team2 < 0) {
            System.out.println("Так не буває, спробуй ще раз!");
            frag1Team2 = scanner.nextInt();
        }

        System.out.println("Введіть кількість фрагів другого гравця команди " + secondTeamName);
        int frag2Team2 = scanner.nextInt();
        while (frag2Team2 < 0) {
            System.out.println("Так не буває, спробуй ще раз!");
            frag2Team2 = scanner.nextInt();
        }

        System.out.println("Введіть кількість фрагів третього гравця команди " + secondTeamName);
        int frag3Team2 = scanner.nextInt();
        while (frag3Team2 < 0) {
            System.out.println("Так не буває, спробуй ще раз!");
            frag3Team2 = scanner.nextInt();
        }

        System.out.println("Введіть кількість фрагів четвертого гравця команди " + secondTeamName);
        int frag4Team2 = scanner.nextInt();
        while (frag4Team2 < 0) {
            System.out.println("Так не буває, спробуй ще раз!");
            frag4Team2 = scanner.nextInt();
        }

        System.out.println("Введіть кількість фрагів п'ятого гравця команди " + secondTeamName);
        int frag5Team2 = scanner.nextInt();
        while (frag5Team2 < 0) {
            System.out.println("Так не буває, спробуй ще раз!");
            frag5Team2 = scanner.nextInt();
        }

        double team1Result = (double) (frag1Team1 + frag2Team1 + frag3Team1 + frag4Team1 + frag5Team1) / 5;
        // System.out.println("Середнє аріфметичне балів команди " + firstTeamName + " = " + team1Result);

        double team2Result = (double) (frag1Team2 + frag2Team2 + frag3Team2 + frag4Team2 + frag5Team2) / 5;
        // System.out.println("Середнє аріфметичне балів команди " + secondTeamName + " = " + team2Result);

        System.out.println(team1Result - team2Result);
        System.out.println(Math.abs(team1Result - team2Result));

        if (0.1 > Math.abs(team1Result - team2Result)) {
            System.out.println("Гра не виявила переможця - нічия! Команди набрали по " + team1Result + " очків!");
        } else if (team1Result > team2Result) {
            System.out.println("Перемогла команда " + firstTeamName + " набрала " + team1Result + " очків");
        } else {
            System.out.println("Перемогла команда " + secondTeamName + " набрала " + team2Result + " очків");
        }
    }
}
