package com.my.olegfp;

import java.util.Scanner;

public class MyNumber {
    public static void main(String[] args) {

        System.out.println("Спробуйте вгадати число, яке я загадав!");

        int minNumber = 0;
        int maxNumber = 10;
        int myNumber;
        myNumber = minNumber + (int) (Math.random() * ((maxNumber - minNumber) + 1));

        System.out.println("Введіть кількість спроб");
        Scanner scanner = new Scanner(System.in);
        int attemptsQuantity = scanner.nextInt();
        while (attemptsQuantity < 3) {
            System.out.println("Дайте собі принаймні три шанси!");
            attemptsQuantity = scanner.nextInt();
        }

        System.out.println("Вгадайте, яке число з диапазону від " + minNumber + " до " + maxNumber + " я загадав! ");
        for (int i = 0; i < attemptsQuantity; i++) {
            int userAttempt = scanner.nextInt();

            while (userAttempt < minNumber) {
                System.out.println("Число не може бути менше " + minNumber);
                userAttempt = scanner.nextInt();
            }

            while (userAttempt > maxNumber) {
                System.out.println("Число не може бути більше " + maxNumber);
                userAttempt = scanner.nextInt();
            }

            if (userAttempt != myNumber) {
                System.out.println("Не вгадали, спробуйте ще раз! ");
            } else {
                System.out.println("Дуже добре, ви вгадали! ");
                break;
            }
        }
        System.out.println("Я загадував число " + myNumber);
    }
}
