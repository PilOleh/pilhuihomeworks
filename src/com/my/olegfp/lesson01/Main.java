package com.my.olegfp.lesson01;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Student student = new Student(38, "Alexa");
        System.out.println("Введіть ім'я студента: ");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        String nameTrue = student.getName();
        while (!name.equals(nameTrue)) {
            System.out.println("Спробуй ще раз: ");
            name = scanner.nextLine();
        }
        System.out.println("Ви вгадали ім'я! Спробуйте вгадати вік! ");
        Scanner scanner1 = new Scanner(System.in);
        int age = scanner1.nextInt();
        int ageTrue = student.getAge();
        while (age != ageTrue) {
            if (age > ageTrue) {
                System.out.println("Вік студента менший, спробуйте ще раз: ");
                age = scanner1.nextInt();
            } else if (age < ageTrue) {
                System.out.println("Вік студента більший, спробуйте ще раз: ");
                age = scanner1.nextInt();
            }
        }

            System.out.println("Ви вгадали вік! Дуже добре! ");

    }
}
