package com.my.olegfp;

import java.util.Scanner;

public class RugbyTeams {
    public static void main(String[] args) {
        System.out.println("Введіть кількість гравців команд");
        Scanner scanner = new Scanner(System.in);
        int playersQuantity = scanner.nextInt();

        int minAge = 18;
        int maxAge = 40;
        int sumAgesTeam1 = 0;
        int sumAgesTeam2 = 0;
        int playerNumber = 0;
        int[] team1Ages = new int[playersQuantity];
        int[] team2Ages = new int[playersQuantity];

        System.out.println("Вік гравців першої команди   " + "Вік гравців другої команди");

        for (int i = 0; i < team1Ages.length; i++) {

            team1Ages[i] = 18 + (int) (Math.random() * ((maxAge - minAge) + 1));
            team2Ages[i] = 18 + (int) (Math.random() * ((maxAge - minAge) + 1));

            sumAgesTeam1 = sumAgesTeam1 + team1Ages[i];
            sumAgesTeam2 = sumAgesTeam2 + team2Ages[i];

            playerNumber++;

            System.out.println("\t \t" + playerNumber + " - " + team1Ages[i] + "\t \t \t \t \t \t" + playerNumber + " - " + team2Ages[i]);

        }

        double middleAgeTeam1 = (double) sumAgesTeam1 / playersQuantity;
        double middleAgeTeam2 = (double) sumAgesTeam2 / playersQuantity;

        System.out.println();
        System.out.println("Cередній вік гравців першої команди - " + middleAgeTeam1);
        System.out.println();
        System.out.println("Cередній вік гравців другої команди - " + middleAgeTeam2);
    }
}
