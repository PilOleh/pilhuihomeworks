package com.my.olegfp;

import java.util.Scanner;

public class MatrixTransposition {
    public static void main(String[] args) {

        System.out.println("Введіть кількість рядків початкової матриці");
        Scanner scanner = new Scanner(System.in);
        int firstMatrixRows = scanner.nextInt();
        System.out.println("Введіть кількість стовбців початкової матриці");
        int firstMatrixColumns = scanner.nextInt();

        int[][] firstMatrix = new int[firstMatrixRows][firstMatrixColumns];


        System.out.println("Початкова матриця");
        for (int i = 0; i < firstMatrix.length; i++) {
            for (int j = 0; j < firstMatrix[i].length; j++) {
                firstMatrix[i][j] = (int) (Math.random() * 100);
                System.out.print(firstMatrix[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();

        int secondMatrixRows = firstMatrixColumns;
        int secondMatrixColumns = firstMatrixRows;
        int[][] secondMatrix = new int[secondMatrixRows][secondMatrixColumns];

        System.out.println("Транспонована матриця");
        for (int i = 0; i < secondMatrix.length; i++) {
            for (int j = 0; j < secondMatrix[i].length; j++) {
                secondMatrix[i][j] = firstMatrix[j][i];
                System.out.print(secondMatrix[i][j] + " ");
            }
            System.out.println();
        }
    }
}
