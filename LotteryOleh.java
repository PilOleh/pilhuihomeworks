package com.my.olegfp;

public class LotteryOleh {
    public static void main(String[] args) {

        int numbersQuantity = 7;
        int[] lotteryOrganizerNumbers = generate(numbersQuantity);
        int[] playerNumbers = generate(numbersQuantity);

        System.out.println("Початкові масиви");
        print(lotteryOrganizerNumbers);
        System.out.println();
        print(playerNumbers);

        arrayBubbleSort(lotteryOrganizerNumbers);
        arrayBubbleSort(playerNumbers);

        System.out.println();
        System.out.println();
        System.out.println("Відсортовані масиви");
        print(lotteryOrganizerNumbers);
        System.out.println();
        print(playerNumbers);
        System.out.println();

        int coincidencesNumber = 0;
        for (int i = 0; i < lotteryOrganizerNumbers.length; i++) {
            if (lotteryOrganizerNumbers[i] == playerNumbers[i]) {
                coincidencesNumber++;
            }
        }
        System.out.println("Кількість збігів = " + coincidencesNumber);
    }


    public static int[] generate(int size) {
        int[] myArray = new int[size];
        for (int i = 0; i < myArray.length; i++) {
            myArray[i] = (int) (Math.random() * 10);
        }
        return myArray;
    }


    public static void arrayBubbleSort(int[] myArray) {
        for (int i = 0; i < myArray.length - 1; i++) {
            for (int j = 0; j < myArray.length - 1; j++) {
                if (myArray[j] > myArray[j + 1]) {
                    int tmp = myArray[j];
                    myArray[j] = myArray[j + 1];
                    myArray[j + 1] = tmp;
                }
            }
        }
    }

    private static void print(int[] myArray) {
        for (int arrayElement : myArray) {
            System.out.print(arrayElement + "  ");
        }
        System.out.println();
    }
}
